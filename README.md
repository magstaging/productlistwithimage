# README #

Create a minimalistic module that builds a block with product listing and each product has an image

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/Mbs_ProductListWithImage when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

every page in the site should show sone products with the images. the variable productIds is hard
coded in the template list.phtml and can be overriden in various ways.